all: compile

compile:
	gcc -o branch predictor.c

bimodalSimple:
	gunzip -c shortened.trace.gz | branch -s 1 -bp 0 -gh 1 -ph 1

clean:
	find . -type f -name '*.exe' -delete
