#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/**
 *	\fn 	readInput
 *	\param	string Un puntero de char que indica el string a leer.
 *	\return	salida Devuelve el entero equivalente al string indicado.
 *	\brief 	Contiene un 'parser' para leer el contenido de uno de los argumentos de línea de comando, y traducirlo a un entero.
 */
int readInput(char* string) {
	// inicializar caracter temp
	char temp;
	int i, size;
	int salida = 0;
	
	// iterar sobre todo el string
	size = (int)strlen(string);

	// iterar todo el string
	for (i=0; i<size; ++i) {
		salida = salida*10;
		temp = (int)(string[i]) - '0';
		salida = salida + temp;
	}
	
	// devolver el nuevo int a la salida
	return salida;
}

/**
 *	\fn 	predictionParameters
 *	\param	predictionType	El tipo de predicción.
 *	\param	bht	La cantidad de entradas al BHT.
 *	\param	gh	El tamaño del registro de historia global.
 *	\param	ph	El tamaño del registro de historia privado.
 *	\return	void
 *	\brief 	Imprime los parámetros de predicción
 */
void predictionParameters(char* predictionType, int bht, int gh, int ph) {
	
	printf("------\n");
	printf("Prediction parameters: \n");
	printf("------\n");
	printf("Branch prediction type: \t\t\t\t%s\n", predictionType);
	printf("BHT size (entries): \t\t\t\t\t%d\n", bht);
	printf("Global history register size: \t\t\t\t%d\n", gh);
	printf("Pivate history register size: \t\t\t\t%d\n", ph);
	printf("------\n");
	
	// return vacío, no se devuelve un valor
	return;
}

/**
 *	\fn 	simulationResults
 *	\param	branchNum	El número de ramas analizadas.
 *	\param	correctT	El número de saltos identificados correctamente.
 *	\param	incorrectT	El número de saltos no identificados.
 *	\param	correctN	El número de no-saltos identificados correctamente.
 *	\param	incorrectN	El número de no-saltos no identificados.
 *	\return	void
 *	\brief 	Imprime los resultados de la simulación
 */
void simulationResults(long int branchNum, int correctT, int incorrectT, int correctN, int incorrectN) {
	
	float percent;
	percent = (float)(correctT+correctN)/(correctT+correctN+incorrectT+incorrectN);
	
	printf("Simulation results: \n");
	printf("------\n");
	printf("Number of branch: \t\t\t\t\t%ld\n", branchNum);
	printf("Number of correct prediction of taken branches: \t%d\n", correctT);
	printf("Number of incorrect prediction of taken branches: \t%d\n", incorrectT);
	printf("Number of correct prediction of not taken branches: \t%d\n", correctN);
	printf("Number of incorrect prediction of not taken branches: \t%d\n", incorrectN);
	printf("Percentage of correct predictions:  \t\t\t%0.2f\n", percent*100);
	printf("------\n");
	
	// return vacío, no se devuelve un valor
	return;
}

/**
 *	\fn 	predictorBimodal
 *	\param	s El parámetro s de entrada que indica el tamaño del arreglo.
 *	\return	void
 *	\brief 	Contiene todo el código necesario para realizar un predictor bimodal con base en los datos ingresados en el standard input.
 */
void predictorBimodal(int s) {
	// Variables de lectura de archivo
	static char line[80];
	char *token, *status;
	
	// Variables del predictor
	int i, previousState, newState, predict;
	long int branchNum = 0;
	unsigned int address, mask, index;
	char instr;
	
	// Variables con valor preset
	int n = s;
	int correctT = 0;
	int correctN = 0;
	int incorrectT= 0;
	int incorrectN = 0;

	// declarar arreglo dinámico usando malloc
	int *table;
	table = (int*)malloc(pow(2,n)*sizeof(int));
	
	// inicializar la tabla
	for (i=0; i<(1 << n); ++i) {
		table[i] = 3; // inicializar todo valor como strongly not taken
	}
		
	// elaborar máscara para extraer los últimos n bits
	mask = (1 << n) - 1;
	
	// leer todas las líneas hasta final del archivo
	do {

		// leer 1 línea de stdin utilizando fgets
		status = fgets(line, 80, stdin);
		
		// condición de salida
		if (status == NULL){
			//printf("condicion de salida \n"); // para depurar
			break;
		}
		
		// utilizar string token para iterar
		// PC address
		token = strtok(line, " ");
		//printf("%s  ", token); // para depurar
		sscanf(token, "%u", &address);		
		// instrucción
		token = strtok(NULL, " ");
		instr = *token;
		
		// índice aplica la máscara con AND lógico
		index = address & mask;
		//printf("%u %d %c ", address, index, instr); //para depurar
		
		// hacer predicción usando tabla
		predict = ( (table[index] == 1) | (table[index] == 0) ); // si 1 o 0, tomar salto. Contrario, no tomar salto.
		
		// actualizar estado de predicción
		previousState = table[index]; 
		if(previousState == 0){ //strongly taken
			if(instr == 'T'){
				newState = 0; // sigue strongly taken
				correctT ++;
			}
			else{
				newState = 1; // ahora weakly taken
				incorrectN ++;
			}
		}
		else if (previousState == 1 ){ //weakly taken
			if(instr == 'T'){
				newState = 0; // ahora strongly taken
				correctT ++;
			}
			else{
				newState = 2; // ahora weakly not taken
				incorrectN ++;
			}
		}
		else if (previousState == 2 ){ // weakly not taken
			if(instr == 'T'){
				newState = 1; // ahora weakly taken
				incorrectT ++;
			}
			else{
				newState = 3; // ahora strongly not taken
				correctN ++;
			}
		}
		else if (previousState == 3 ){ // strongly not taken
			if(instr == 'T'){
				newState = 2; // ahora weakly not taken
				incorrectT ++;
			}
			else{
				newState = 3; // sigue strongly not taken
				correctN ++;
			}
		}
		// actualizar tabla y cantidad de saltos
		table[index] = newState;
		branchNum ++;
		
		//printf("%d \n", newState); // para depurar
		
		
	} while ( status != NULL );
	
	// imprimir los resultados
	simulationResults(branchNum,correctT,incorrectT,correctN,incorrectN);

	// liberar el malloc de la tabla
	free (table);
	
	// return vacío, no se devuelve un valor
	return;
}


/**
 *	\fn 	predictorHistoriaGlobal
 *	\param	gh La cantidad de bits de patrón que se analiza.
 *	\return	void
 *	\brief 	Contiene todo el código necesario para realizar predicciónes a escala global con respecto a un patrón de gh bits.
 */
void predictorHistoriaGlobal(int gh) {
	// Variables de lectura de archivo
	static char line[80];
	char *token, *status;
	
	// Variables del predictor
	int predict, previousState, newState, i;
	unsigned int address;
	long int branchNum = 0;
	char instr;
	
	// Variables con valor preset
	unsigned int history = 0; // asumir que nunca se tomó un salto anteriormente
	unsigned int mask = 0; // inicializar máscara en 0 (todo vacío)
	int s = gh; // asumir tablas de tamaño ideal, tal que s = gh
	int correctT = 0;
	int correctN = 0;
	int incorrectT= 0;
	int incorrectN = 0;
	
	// declarar arreglos dinámicos usando malloc
	int *predictionTable;
	predictionTable = (int*)malloc(pow(2,s)*sizeof(int));
	
	// inicializar máscara para PHT
	mask = (1 << gh) - 1; //shift logical left gh veces (representar gh bits) y restar 1 para crear máscara
	
	// inicializar las tablas
	for (i=0; i<(pow(2,s)); ++i) {
		predictionTable[i] = 3; // inicializar todo valor como strongly not taken
	}
		
	// leer todas las líneas hasta final del archivo
	do {

		// leer 1 línea de stdin utilizando fgets
		status = fgets(line, 80, stdin);
		
		// condición de salida
		if (status == NULL){
			//printf("condicion de salida \n"); // para depurar
			break;
		}
		
		// utilizar string token para iterar
		// PC address
		token = strtok(line, " ");
		//printf("%s  ", token); // para depurar
		sscanf(token, "%u", &address);		
		// instrucción
		token = strtok(NULL, " ");
		instr = *token;
		
		// obtener el estado de predicción anterior
		previousState = predictionTable[history];
		
		// hacer predicción usando tabla
		predict = ( (previousState == 1) | (previousState == 0) ); // si 1 o 0, tomar salto. Contrario, no tomar salto.
		
		// actualizar estado de predicción
		if(previousState == 0){ //strongly taken
			if(instr == 'T'){
				newState = 0; // sigue strongly taken
				correctT ++;
			}
			else{
				newState = 1; // ahora weakly taken
				incorrectN ++;
			}
		}
		else if (previousState == 1 ){ //weakly taken
			if(instr == 'T'){
				newState = 0; // ahora strongly taken
				correctT ++;
			}
			else{
				newState = 2; // ahora weakly not taken
				incorrectN ++;
			}
		}
		else if (previousState == 2 ){ // weakly not taken
			if(instr == 'T'){
				newState = 1; // ahora weakly taken
				incorrectT ++;
			}
			else{
				newState = 3; // ahora strongly not taken
				correctN ++;
			}
		}
		else if (previousState == 3 ){ // strongly not taken
			if(instr == 'T'){
				newState = 2; // ahora weakly not taken
				incorrectT ++;
			}
			else{
				newState = 3; // sigue strongly not taken
				correctN ++;
			}
		}
		// actualizar tabla y cantidad de saltos
		predictionTable[history] = newState;
		branchNum ++;
		
		// actualizar historia (nuevo patrón)
		history = (history << 1) & mask;
		if(instr == 'T'){
			history ++; //1 indica 'taken'
		}
		
		//printf("%d \n", newState); // para depurar
		
		
	} while ( status != NULL );
	
	// imprimir los resultados
	simulationResults(branchNum,correctT,incorrectT,correctN,incorrectN);
	
	// liberar el espacio reservado en las tablas
	free (predictionTable);
	
	// return vacío, no se devuelve un valor
	return;
}

/**
 *	\fn 	predictorHistoriaPrivada
 *	\param	s	Cantidad de bits de 'address' que se desea trabajar.
 *	\param	ph	Cantidad de bits de historia del predictor local.
 *	\return	void
 *	\brief 	Contiene todo el código necesario para realizar predicciónes utilizando la historia de un set de direcciones.
 */
void predictorHistoriaPrivada(int s, int ph) {
	// Variables de lectura de archivo
	static char line[80];
	char *token, *status;
	
	// Variables del predictor
	int predict, previousState, newState, i, j;
	unsigned int address, pattern, index;
	long int branchNum = 0;
	char instr;
	
	// Variables con valor preset
	unsigned int localHistory = 0; // asumir que nunca se tomó un salto anteriormente
	unsigned int mask = 0; // inicializar máscara en 0 (todo vacío)
	int correctT = 0;
	int correctN = 0;
	int incorrectT= 0;
	int incorrectN = 0;
	unsigned int phBytes = pow(2,ph);
	unsigned int sBytes = pow(2,s);
	
	// declarar arreglos dinámicos usando malloc
	int **predictionTable, *branchHistoryTable;
	branchHistoryTable = (int*)malloc(sBytes*sizeof(int));
	predictionTable = (int**)malloc(sBytes*sizeof(int *));
	for (i=0; i<sBytes; ++i){
		predictionTable[i] = (int*)malloc(phBytes*sizeof(int));
	}
	
	// inicializar máscara para PHT
	mask = (1 << s) - 1; //shift logical left s veces (representar s bits) y restar 1 para crear máscara de direccionamiento
	
	// inicializar las tablas
	for (i=0; i<(sBytes); ++i) {
		branchHistoryTable[i] = 0; // asumimos que toda rama no ha tomado anteriormente un salto
		for (j=0; j<(phBytes); ++j){
			predictionTable[i][j]=3; // inicializar todo valor como strongly not taken
		}
	}
		
	// leer todas las líneas hasta final del archivo
	while ( status != NULL ) {

		// leer 1 línea de stdin utilizando fgets
		status = fgets(line, 80, stdin);
		
		// condición de salida
		if (status == NULL){
			//printf("condicion de salida \n"); // para depurar
			break;
		}
		
		// utilizar string token para iterar
		// PC address
		token = strtok(line, " ");
		//printf("%s  ", token); // para depurar
		sscanf(token, "%u", &address);		
		// instrucción
		token = strtok(NULL, " ");
		instr = *token;
		
		// índice aplica la máscara con AND lógico
		index = address & mask;
		
		// obtener historia de esta dirección (masked)
		localHistory = branchHistoryTable[index];
		
		// obtener el estado de predicción anterior (basado en la historia)
		previousState = predictionTable[index][localHistory];
		
		
		// hacer predicción usando tabla
		predict = ( (previousState == 1) | (previousState == 0) ); // si 1 o 0, tomar salto. Contrario, no tomar salto.
		
		// actualizar estado de predicción
		if(previousState == 0){ //strongly taken
			if(instr == 'T'){
				newState = 0; // sigue strongly taken
				correctT ++;
			}
			else{
				newState = 1; // ahora weakly taken
				incorrectN ++;
			}
		}
		else if (previousState == 1 ){ //weakly taken
			if(instr == 'T'){
				newState = 0; // ahora strongly taken
				correctT ++;
			}
			else{
				newState = 2; // ahora weakly not taken
				incorrectN ++;
			}
		}
		else if (previousState == 2 ){ // weakly not taken
			if(instr == 'T'){
				newState = 1; // ahora weakly taken
				incorrectT ++;
			}
			else{
				newState = 3; // ahora strongly not taken
				correctN ++;
			}
		}
		else if (previousState == 3 ){ // strongly not taken
			if(instr == 'T'){
				newState = 2; // ahora weakly not taken
				incorrectT ++;
			}
			else{
				newState = 3; // sigue strongly not taken
				correctN ++;
			}
		}
		
		// actualizar tabla y cantidad de saltos
		predictionTable[index][localHistory] = newState;
		branchNum ++;
		
		// actualizar historia (nuevo patrón)
		localHistory = (localHistory << 1) & mask;
		if(instr == 'T'){
			localHistory ++; //1 indica 'taken'
		}
		
		// actualizar tabla de direcciones con el nuevo dato
		branchHistoryTable[index] = localHistory;
		
		//printf("%d \n", newState); // para depurar
		
	}
	
	// imprimir los resultados
	simulationResults(branchNum,correctT,incorrectT,correctN,incorrectN);
	
	// liberar el espacio reservado en las tablas
	free (branchHistoryTable);
	for (i=0; i<sBytes; ++i){
		//printf("address: %p\n",predictionTable[i]);
		free (predictionTable[i]);
	}
	free (predictionTable);
	
	// return vacío, no se devuelve un valor
	return;
}

/**
 *	\fn 	predictorTorneo
 *	\param	s	Cantidad de bits de 'address' que se desea trabajar.
 *	\param	gh	La cantidad de bits de patrón que se analiza.
 *	\param	ph	Cantidad de bits de historia del predictor local.
 *	\return	void
 *	\brief 	Contiene todo el código necesario para realizar las predicciónes Pshare y Gshare, mezcladas en un metapredictor Torneo.
 */
void predictorTorneo(int s, int gh, int ph) {
	// Variables de lectura de archivo
	static char line[80];
	char *token, *status;
	
	// Variables del predictor
	int predict, localPreviousState, globalPreviousState, localNewState, globalNewState, i, j, newMetaPrediction;
	unsigned int address, pattern, index, addressMask, ghMask;
	long int branchNum = 0;
	char instr;
	
	// Variables con valor preset
	unsigned int localHistory = 0; // asumir que nunca se tomó un salto anteriormente
	unsigned int globalHistory = 0; // asumir que nunca se tomó un salto anteriormente
	int metaPrediction = 0; // asumir que se inicializa en 0 (strongly prefer Pshare)
	int correctT = 0;
	int correctN = 0;
	int incorrectT= 0;
	int incorrectN = 0;
	unsigned int phBytes = pow(2,ph);
	unsigned int sBytes = pow(2,s);
	
	// declarar arreglos dinámicos usando malloc
	int **localPredictionTable, *branchHistoryTable, *globalPredictionTable, *metaPredictor;
	branchHistoryTable = (int*)malloc(sBytes*sizeof(int));
	localPredictionTable = (int**)malloc(sBytes*sizeof(int *));
	globalPredictionTable = (int*)malloc(sBytes*sizeof(int *));
	metaPredictor = (int*)malloc(sBytes*sizeof(int));
	for (i=0; i<sBytes; ++i){
		localPredictionTable[i] = (int*)malloc(phBytes*sizeof(int));
		globalPredictionTable[i] = 3;
		metaPredictor[i] = 0; // iniciar en 0 (strongly prefer pshare)
	}
	
	// inicializar máscara para tabla de predicciones locales
	addressMask = (1 << s) - 1; //shift logical left s veces (representar s bits) y restar 1 para crear máscara de direccionamiento
	// inicializar máscara para tabla de prediciones globales
	ghMask = (1 << gh) - 1; //shift logical left gh veces (representar gh bits) y restar 1 para crear máscara
	
	// inicializar las tablas
	for (i=0; i<(sBytes); ++i) {
		branchHistoryTable[i] = 0; // asumimos que toda rama no ha tomado anteriormente un salto
		for (j=0; j<(phBytes); ++j){
			localPredictionTable[i][j]=3; // inicializar todo valor como strongly not taken
		}
	}	
	
	
	// leer todas las líneas hasta final del archivo
	while ( status != NULL ) {

		// leer 1 línea de stdin utilizando fgets
		status = fgets(line, 80, stdin);
		
		// condición de salida
		if (status == NULL){
			//printf("condicion de salida \n"); // para depurar
			break;
		}
		
		// utilizar string token para iterar
		// PC address
		token = strtok(line, " ");
		//printf("%s  ", token); // para depurar
		sscanf(token, "%u", &address);		
		// instrucción
		token = strtok(NULL, " ");
		instr = *token;
		
		// índice aplica la máscara de la dirección con AND lógico
		index = address & addressMask;
		
		// obtener historia de esta dirección (masked) y estado del metapredictor
		localHistory = branchHistoryTable[index];
		metaPrediction = metaPredictor[index];
		newMetaPrediction = metaPrediction; //asumimos que no cambia el metapredictor
		
		// obtener el estado de predicción anterior (basado en la historia)
		localPreviousState = localPredictionTable[index][localHistory];
		globalPreviousState = globalPredictionTable[globalHistory];
		
		if ((metaPrediction == 0)|(metaPrediction == 1)){ // preferencia, Pshare
			predict = ( (localPreviousState == 1) | (localPreviousState == 0) ); // si 1 o 0, tomar salto. Contrario, no tomar salto.
		}
		else{ // preferencia Gshare
			predict = ( (globalPreviousState == 1) | (globalPreviousState == 0) ); // si 1 o 0, tomar salto. Contrario, no tomar salto.
		}
		
		// actualizar estado de predicción para ambas tablas
		if(instr == 'T'){ // (-1 si posible)
			// revisar predicciones locales
			if(localPreviousState == 0){
				localNewState = 0;
			}
			else if(localPreviousState == 1){
				localNewState = 0;
			}
			else if(localPreviousState == 2){
				localNewState = 1;
			}
			else if(localPreviousState == 3){
				localNewState = 2;
			}
			// revisar predicciones globales
			if(globalPreviousState == 0){
				globalNewState = 0;
			}
			else if(globalPreviousState == 1){
				globalNewState = 0;
			}
			else if(globalPreviousState == 1){
				globalNewState = 1;
			}
			else if(globalPreviousState == 1){
				globalNewState = 2;
			}
			// revisar metapredictor
			if(predict == 1){
				correctT ++;
			}
			else if(predict == 0){
				incorrectT ++;
			}
			// actualizar metapredictor
			// pShare es correcto, gShare es incorrecto, y no se está en estado "strongly prefer pshare = 0"
			if(((localPreviousState == 0)|(localPreviousState == 1))&((globalPreviousState == 2)|(globalPreviousState == 3))&(metaPrediction!=0)){
				metaPrediction --; // acercar 1 nivel de preferencia hacia pShare
			}
			// pShare es incorrecto, gShare es correcto, y no se está en estaedo "strongly prefer gshare = 3"
			else if(((localPreviousState == 2)|(localPreviousState == 3))&((globalPreviousState == 0)|(globalPreviousState == 1))&(metaPrediction!=3)){
				metaPrediction ++; // acercar 1 nivel de preferencia hacia gShare
			}
		}
		else{ //es 'F' (+1 si posible)
			// revisar predicciones locales
			if(localPreviousState == 0){
				localNewState = 1;
			}
			else if(localPreviousState == 1){
				localNewState = 2;
			}
			else if(localPreviousState == 2){
				localNewState = 3;
			}
			else if(localPreviousState == 3){
				localNewState = 3;
			}
			// revisar predicciones globales
			if(globalPreviousState == 0){
				globalNewState = 1;
			}
			else if(globalPreviousState == 1){
				globalNewState = 2;
			}
			else if(globalPreviousState == 2){
				globalNewState = 3;
			}
			else if(globalPreviousState == 3){
				globalNewState = 3;
			}
			// revisar metapredictor
			if(predict == 1){
				incorrectN ++;
			}
			else if(predict == 0){
				correctN ++;
			}
			// actualizar metapredictor
			// pShare es incorrecto, gShare es correcto, y no se está en estado "strongly prefer gshare = 3"
			if(((localPreviousState == 0)|(localPreviousState == 1))&((globalPreviousState == 2)|(globalPreviousState == 3))&(metaPrediction!=3)){
				metaPrediction ++; // acercar 1 nivel de preferencia hacia gShare
			}
			// pShare es correcto, gShare es incorrecto, y no se está en estaedo "strongly prefer pshare = 0"
			else if(((localPreviousState == 2)|(localPreviousState == 3))&((globalPreviousState == 0)|(globalPreviousState == 1))&(metaPrediction!=0)){
				metaPrediction --; // acercar 1 nivel de preferencia hacia pShare
			}
		}	
		
		
		
		// actualizar tablas y cantidad de saltos
		localPredictionTable[index][localHistory] = localNewState;
		globalPredictionTable[globalHistory] = globalNewState;
		metaPredictor[index] = metaPrediction;
		branchNum ++;
		
		// actualizar historia local (nuevo patrón)
		localHistory = (localHistory << 1) & addressMask;
		if(instr == 'T'){
			localHistory ++; //1 indica 'taken'
		}
		
		// actualizar tabla de direcciones con el nuevo dato
		branchHistoryTable[index] = localHistory;
		
		// actualizar historia global (nuevo patrón)
		globalHistory = (globalHistory << 1) & ghMask;
		if(instr == 'T'){
			globalHistory ++; //1 indica 'taken'
		}
		
		//printf("%d \n", newState); // para depurar
		
	}

	
	// imprimir los resultados
	simulationResults(branchNum,correctT,incorrectT,correctN,incorrectN);
	
	// liberar el espacio reservado en las tablas
	free (branchHistoryTable);
	for (i=0; i<sBytes; ++i){
		//printf("address: %p\n", localPredictionTable[i]);
		free (localPredictionTable[i]);
	}
	free (localPredictionTable);	
	free (globalPredictionTable);
	free (metaPredictor);
	
	// return vacío, no se devueve un valor
	return;
}


/**
 *	\fn 	main
 *	\param	argc	El número de argumentos que se pasa por la terminal.
 *	\param	argv[]	Arreglo de punteros char que contiene los argumentos.
 *	\return	void
 *	\brief	Contiene todo el código para llamar a otras funciones, y comprobar el funcionamiento de los predictores.
 */
void main(int argc, char *argv[])
{
	// Declaración de variables
	int i;
	// Valores por defecto de parámetros por la terminal
	int s = 2;
	int bp = 4;
	int gh = 2;
	int ph = 2;
	char *ptr;
	
	// lectura de entradas por argv	
	for(i=0; i<argc; i++){
		// obtener puntero correspondiente a este argumento particular
		ptr = argv[i];
		// revisar casos importantes
		if((*ptr=='-')&(*(ptr+1)=='s')){
			s = readInput(argv[i+1]);
		}
		else if((*ptr=='-')&(*(ptr+1)=='b')&(*(ptr+2)=='p')){
			bp = readInput(argv[i+1]);
		}
		else if((*ptr=='-')&(*(ptr+1)=='g')&(*(ptr+2)=='h')){
			gh = readInput(argv[i+1]);
		}
		else if((*ptr=='-')&(*(ptr+1)=='p')&(*(ptr+2)=='h')){
			ph = readInput(argv[i+1]);
		}
	}
	
	// imprimir las entradas
	// printf("s = %d, bp = %d, gh = %d, ph = %d \n",s,bp,gh,ph); //para depurar
	
	// llamar al predictor correspondiente 
	if(bp==0){
		// printf("Predictor Bimodal \n"); // para depurar
		predictionParameters("Bimodal",pow(2,s),gh,ph);
		predictorBimodal(s);
	}
	else if(bp==1){
		// printf("Predictor con historia privada \n"); // para depurar
		predictionParameters("PShare",pow(2,s),gh,ph);
		predictorHistoriaPrivada(s, ph);
	}
	else if(bp==2){
		// printf("Predictor con historia global \n"); // para depurar
		predictionParameters("GShare",pow(2,s),gh,ph);
		predictorHistoriaGlobal(gh);
	}
	else if(bp==3){
		// printf("Predictor con torneo \n"); // para depurar
		predictionParameters("Tournament",pow(2,s),gh,ph);
		predictorTorneo(s, gh, ph);
	}
	else{
		printf("Predictor desconocido, error de ingreso de variables \n");
	}
	
}