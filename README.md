# IE0521_Tarea1

Este repositorio contiene la Tarea 1 de Estructuras de Computadoras Digitales II, realizado por Laura Rojas (B76798).

# Contenido

Contiene un Makefile que con la instrucción 'make compile' compila el predictor de saltos (predictor.c) en un ejecutable llamado 'branch'.

Se incluyen los archivos comprimidos y descomprimidos branch-trace-gcc.trace y shortened.trace que son el archivo de prueba, y un archivo reducido con solo 200 saltos, utilizado para varias pruebas.

La instrucción 'make bimodalSimple' corre el predictor en modo bimodal, con variables pequeñas, utilizando este archivo de prueba reducido.

Este predictor debe correrse de la forma indicada en el enuncidao.


